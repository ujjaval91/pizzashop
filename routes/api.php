<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('getPizzas', 'PizzasController@index');
Route::post('order', 'OrdersController@create');
Route::post('orders', 'OrdersController@getOrders');
Route::get('pizza/{id}', 'PizzasController@getpizza');
Route::post('login', 'UserController@login');
Route::post('email-verify', 'UserController@emailVerify');
Route::post('register', 'UserController@register');



// Route::group(['middleware' => 'auth:api'], function()
// {
//    Route::post('details', 'UserController@details');
// });