-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 17, 2020 at 02:50 PM
-- Server version: 8.0.18
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizza_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ingredients`
--

DROP TABLE IF EXISTS `ingredients`;
CREATE TABLE IF NOT EXISTS `ingredients` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ingredients`
--

INSERT INTO `ingredients` (`id`, `title`, `price`, `created_at`, `updated_at`) VALUES
(1, 'Extra Cheese', 10.00, '2020-05-13 18:30:00', '2020-05-13 18:30:00'),
(2, 'Mushrooms', 8.00, '2020-05-13 18:30:00', '2020-05-13 18:30:00'),
(3, 'Pineapple', 6.00, '2020-05-13 18:30:00', '2020-05-13 18:30:00'),
(4, 'Pepperoni', 6.00, '2020-05-13 18:30:00', '2020-05-13 18:30:00'),
(5, 'Peppers', 10.30, '2020-05-13 18:30:00', '2020-05-13 18:30:00'),
(6, 'Onions', 3.00, '2020-05-13 18:30:00', '2020-05-13 18:30:00'),
(7, 'Spinach', 3.00, '2020-05-13 18:30:00', '2020-05-13 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(3, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(5, '2016_06_01_000004_create_oauth_clients_table', 1),
(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(7, '2019_08_19_000000_create_failed_jobs_table', 1),
(8, '2020_05_13_130018_create_pizzas_table', 2),
(9, '2020_05_14_103521_create_ingredients_table', 3),
(10, '2020_05_17_064728_create_orders_table', 4),
(12, '2020_05_17_065743_create_user_items_table', 5),
(13, '2020_05_17_090941_create_user_item__ingredients_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('9c2d2c91f7720e50688de4218f78cc6bdca8a840c9c872d7140c8b050f2e67c2ac281e20465a7750', 2, 5, 'MyApp', '[]', 0, '2020-05-11 07:45:59', '2020-05-11 07:45:59', '2021-05-11 13:15:59'),
('b59c21013de39dbf08283005f11b935e60eed35454fbf4577ca1c1612533c5efbefdc05371b623d7', 3, 5, 'MyApp', '[]', 0, '2020-05-11 07:46:15', '2020-05-11 07:46:15', '2021-05-11 13:16:15'),
('84591f0b90b9a28846b6b0c5f963773392810531f13760fcda013c860b74bfb194342552fc044a1f', 4, 5, 'MyApp', '[]', 0, '2020-05-11 07:49:15', '2020-05-11 07:49:15', '2021-05-11 13:19:15'),
('ada0447f3a0a083c529f6191d4e4477bdf515ef33d4796465c3c3b3c829d047072b4a7e57861bbae', 5, 5, 'MyApp', '[]', 0, '2020-05-11 08:01:51', '2020-05-11 08:01:51', '2021-05-11 13:31:51'),
('fd7bf43403b94979de9131e650ee589c71f7fecd5b8f150f02c2e8aef2f60686eeaef2bc9ab55620', 6, 5, 'MyApp', '[]', 0, '2020-05-11 08:04:09', '2020-05-11 08:04:09', '2021-05-11 13:34:09'),
('87fe3e204f669c7c68c1abbfd4543627610a29573ff34a837c5afe9c00d0770ad009d7aa1dd41e89', 7, 5, 'MyApp', '[]', 0, '2020-05-11 12:42:21', '2020-05-11 12:42:21', '2021-05-11 18:12:21'),
('fa3c4c9f26a68ed197af042b7c7498f5070bdf3950b289a4468ed7c8699fbe05e0e94d204567c5ee', 6, 5, 'MyApp', '[]', 0, '2020-05-11 13:09:29', '2020-05-11 13:09:29', '2021-05-11 18:39:29'),
('6621c274e9036c5e894f87c0ce4136a655668a667e251129dfca4eda05674b74e91e55194482366e', 6, 5, 'MyApp', '[]', 0, '2020-05-11 13:14:07', '2020-05-11 13:14:07', '2021-05-11 18:44:07'),
('1b235bb5040e6b131959f6672c31f3cb14e1cae793371c6ceb6db97b8b3bb1edff8cc99ef8994b4d', 6, 5, 'MyApp', '[]', 0, '2020-05-11 13:16:20', '2020-05-11 13:16:20', '2021-05-11 18:46:20'),
('f2d11378e06eca1a06b76475f22a9e05e7ce84a7ecb6a11bb9e850d185d859b2395dc320542d55e8', 6, 5, 'MyApp', '[]', 0, '2020-05-12 02:42:43', '2020-05-12 02:42:43', '2021-05-12 08:12:43'),
('0279a73e5024c5354f701f0d29ab42fd1a4ca757ea588982ad5abe9b069baca931620d6c4e181b51', 6, 5, 'MyApp', '[]', 0, '2020-05-12 11:26:56', '2020-05-12 11:26:56', '2021-05-12 16:56:56'),
('49915261b9db4484cd34bb035b4e92643b9552913e92e0c5e6f6ffadcc7ae44513067b839fc953ba', 6, 5, 'MyApp', '[]', 0, '2020-05-12 11:27:09', '2020-05-12 11:27:09', '2021-05-12 16:57:09'),
('c4b603b4204825647c16e69580456c55c0eb4abba9d91da06eff04b63cd60af468e276e7dd26f7ef', 8, 5, 'MyApp', '[]', 0, '2020-05-13 00:42:18', '2020-05-13 00:42:18', '2021-05-13 06:12:18'),
('4d8c753e5a0d1352ac6b75a6772f33aad26a4ecc662f6724d86740e5b8e47e1dd42f340b168311fe', 8, 5, 'MyApp', '[]', 0, '2020-05-13 00:47:32', '2020-05-13 00:47:32', '2021-05-13 06:17:32'),
('c28c5ab23983e98fb6708f0a9cca8020a4f41485d355c2fa1f7253c8ebe4faad5029b490459a65eb', 8, 5, 'MyApp', '[]', 0, '2020-05-13 00:49:37', '2020-05-13 00:49:37', '2021-05-13 06:19:37'),
('9e33a2c1fda9cde5f53e7dde86f854e4d7a1ae77dca40b2d9b439223341229c1ce0253dcf6326a68', 8, 5, 'MyApp', '[]', 0, '2020-05-16 00:12:44', '2020-05-16 00:12:44', '2021-05-16 05:42:44'),
('786293fa7ca9cbd7ec3de861ed6f5a7d4b38ec24029bc62047bb415da95b97baeb7f9637eec341c3', 8, 5, 'MyApp', '[]', 0, '2020-05-16 01:27:31', '2020-05-16 01:27:31', '2021-05-16 06:57:31'),
('c78331c82e4b53e3fe0709eb4fd0ad6e0838d80017e53bc8bb64b24684d7bd02fd6a3b9b59142805', 8, 5, 'MyApp', '[]', 0, '2020-05-16 01:28:08', '2020-05-16 01:28:08', '2021-05-16 06:58:08'),
('e3e3ee7ca65212b933db08f5d353c91dd7caa98ed6c04d4b25625f07ed6586682d292ab826de1158', 8, 5, 'MyApp', '[]', 0, '2020-05-17 05:33:06', '2020-05-17 05:33:06', '2021-05-17 11:03:06'),
('cb00f58b44b5d57a1e3f42e57ddebf8afc059a961aca05528fd884a807385d7991ddb95b5ea4664e', 24, 5, 'MyApp', '[]', 0, '2020-05-17 08:04:10', '2020-05-17 08:04:10', '2021-05-17 13:34:10'),
('404b5bb769e877dbca20f7c1d3b599d7304724438737c14988dc25242af950800354c613ee581ed5', 24, 5, 'MyApp', '[]', 0, '2020-05-17 08:17:42', '2020-05-17 08:17:42', '2021-05-17 13:47:42');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'jXXGz89tISVjWc5VaH1OLdbZW8WrwYuXPSnhb91B', NULL, 'http://localhost', 1, 0, 0, '2020-05-11 07:37:44', '2020-05-11 07:37:44'),
(2, NULL, 'Laravel Password Grant Client', 'ivMzTOP1U4fcNRgvSNJsAlaQ1luVSAxsDGZb8RIl', 'users', 'http://localhost', 0, 1, 0, '2020-05-11 07:37:44', '2020-05-11 07:37:44'),
(3, NULL, 'Laravel Personal Access Client', 'Og9UnRQ59TOeLx9mh9pHBdbUBF0sj8WR9PFHPTuc', NULL, 'http://localhost', 1, 0, 0, '2020-05-11 07:43:27', '2020-05-11 07:43:27'),
(4, NULL, 'Laravel Password Grant Client', 'Qu7PJ4YgUnvkUgZAzAizZlz8BhBAwwPX7HW73NCA', 'users', 'http://localhost', 0, 1, 0, '2020-05-11 07:43:27', '2020-05-11 07:43:27'),
(5, NULL, 'Laravel Personal Access Client', 'iwv3QXGwIf9oq6Y5XbD4lCpRrFMJZsZ10BBGMvbS', NULL, 'http://localhost', 1, 0, 0, '2020-05-11 07:44:44', '2020-05-11 07:44:44');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-05-11 07:37:44', '2020-05-11 07:37:44'),
(2, 3, '2020-05-11 07:43:27', '2020-05-11 07:43:27'),
(3, 5, '2020-05-11 07:44:44', '2020-05-11 07:44:44');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_no` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `price` double(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pizzas`
--

DROP TABLE IF EXISTS `pizzas`;
CREATE TABLE IF NOT EXISTS `pizzas` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_order` smallint(6) NOT NULL,
  `price` double(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pizzas`
--

INSERT INTO `pizzas` (`id`, `title`, `description`, `image`, `display_order`, `price`, `created_at`, `updated_at`) VALUES
(1, 'Margherita', 'Classic delight with 100% real mozzarella cheese', 'margherita.jpg', 5, 199.00, '2020-05-11 18:30:00', '2020-05-12 18:30:00'),
(2, 'Farmhouse', 'Delightful combination of onion, capsicum, tomato & grilled mushroom', 'farmhouse.jpg', 2, 395.00, '2020-05-12 18:30:00', '2020-05-12 18:30:00'),
(3, 'Cheese n Corn', 'A delectable combination of sweet & juicy golden corn', 'cheese_n_corn.jpg', 3, 305.00, '2020-05-12 18:30:00', '2020-05-12 18:30:00'),
(4, 'Pepper Barbecue Chicken', 'Pepper barbecue chicken for that extra zing', 'pepper_barbeque_chicken.jpg', 4, 335.00, '2020-05-12 18:30:00', '2020-05-12 18:30:00'),
(5, 'Peppy Paneer', 'Flavorful trio of juicy paneer, crisp capsicum with spicy red paprika', 'peppy_paneer.jpg', 5, 395.00, '2020-05-12 18:30:00', '2020-05-12 18:30:00'),
(6, 'Veggie Paradise', 'The awesome foursome! Golden corn, black olives, capsicum, red paprika', 'veggie_paradise.jpg', 6, 395.00, '2020-05-12 18:30:00', '2020-05-12 18:30:00'),
(7, 'Veg Extravaganza', 'Black olives, capsicum, onion, grilled mushroom, corn, tomato, jalapeno & extra cheese', 'veg_extravaganza.jpg', 7, 450.00, '2020-05-12 18:30:00', '2020-05-12 18:30:00'),
(8, 'Chicken Sausage', 'American classic! Spicy, herbed chicken sausage on pizza', 'chicken_sausage.jpg', 8, 305.00, '2020-05-12 18:30:00', '2020-05-12 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('M','F','O') COLLATE utf8mb4_unicode_ci DEFAULT 'O',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `gender`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'ujjaval', 'vaishnav.ujjaval1@gmail.com', NULL, '$2y$10$xSepmAIJlq1chj.S2UOev.kazfVsmB7z81ClOhxTIC9kHUnr2HPBC', 'O', NULL, '2020-05-11 07:35:36', '2020-05-11 07:35:36'),
(2, 'ujjaval', 'vaishnav.ujjaval@gmail.com', NULL, '$2y$10$MNhNRTtNtiyHgJslFFf5qO04TkqPt0UUWvJOv9KwZpuwdI35EdVte', 'M', NULL, '2020-05-11 07:45:58', '2020-05-11 07:45:58'),
(3, 'sdsdas', 'saasdsadsa@dsfaf.sasdsad', NULL, '$2y$10$EhPV6filOlEq8COvNErOG.vzPxpK72JPrL3gn4OpJ6eng9Rs07i1m', 'M', NULL, '2020-05-11 07:46:15', '2020-05-11 07:46:15'),
(4, 'asdsd', 'asddas@asdd.asdsad', NULL, '$2y$10$Qp/XJ5LO7kS.ulo0kZNb7eVd4UoYOvOZHND6uMP0ysA/tQ9LW3V66', 'M', NULL, '2020-05-11 07:49:15', '2020-05-11 07:49:15'),
(5, 'asdssadsa', 'dasdasdas@sdasdas.asdsad', NULL, '$2y$10$JoHBbgPph0bDzj6MclW8COL03sb2S5LodIbgbObZ2X3jUmdWrnyHi', 'M', NULL, '2020-05-11 08:01:51', '2020-05-11 08:01:51'),
(6, 'sdfdsfds', 'test@gmail.com', NULL, '$2y$10$wcixFZae49L1oQt4CuFIAO2yTNRsbiaYGd1Ju4m5r7wXGS4NUA9vC', 'M', NULL, '2020-05-11 08:04:09', '2020-05-11 08:04:09'),
(7, 'sdfdfsdfsdf', 'sdfdsfsdfsddfsdfsdfsdffsd@asaddsddas.asdsddsadsa', NULL, '$2y$10$9cz2VNmYwJPmtFhHFEm/EezeOhaQUtnorsKSw6rgottLqhWcmwyEC', 'M', NULL, '2020-05-11 12:42:21', '2020-05-11 12:42:21'),
(8, 'dsadsadsa', 'test@test.com', NULL, '$2y$10$UMfw/.0F8TNNZBKv.wGYnOLKzc/SluzXWf.FXxSpB4dj6.gpFDBZe', 'M', NULL, '2020-05-12 02:55:37', '2020-05-12 02:55:37'),
(9, 'dsfdsfd', 'sd@fdsf.sfds', NULL, '$2y$10$whYQ.ZeGfWGTfnTjO4P01.8G0qxGtiItqDoIORSNkRjFJ2xqEWq.G', 'M', NULL, '2020-05-12 06:50:37', '2020-05-12 06:50:37'),
(10, 'dfgfdgdf', 'sdfsdf@sdfdsfdsf.dfgfdgfd.sfde', NULL, '$2y$10$53u9Q8lUFQfZSwDbit2iO.NAwXF4qE.QBuQ8NWfG9QPEKnyZI3M.a', 'M', NULL, '2020-05-12 06:52:00', '2020-05-12 06:52:00'),
(11, 'dsdsfdsf', 'dsfdsfdsfds@adsffsddsdfdsf.sdfdsfds', NULL, '$2y$10$vtdqEvntW0pLzKUj//xFX.HUsg/GlOwNyMoH0N5jbf8qkU3mkYmzu', 'M', NULL, '2020-05-12 06:53:27', '2020-05-12 06:53:27'),
(12, 'dfgdfgdgf', 'dfdsfdsf@dfgfgfdgf.gfdgdfg', NULL, '$2y$10$7BOCt7ncphutaeTdjF/OhuZhLPq5bJ2e91fLfZsgMtVtIs03dE9yy', 'M', NULL, '2020-05-12 07:25:45', '2020-05-12 07:25:45'),
(13, 'dfgfdgfg', 'gfdgfdg@sddsf.dfgdfgfd', NULL, '$2y$10$GOBlRpWDPr1OXLuWIM/RTubXIUCPYuMusl7i6Hqe47NcJ.Jb36dAK', 'M', NULL, '2020-05-12 07:26:22', '2020-05-12 07:26:22'),
(14, 'dsfdsfdsf', 'fsdfdfds@sdfdf.dfgfgfg', NULL, '$2y$10$JRVKQM/Cu8ylLMx2xwWuze0XHDB4WKX0cCiVyPoqF84kNpC8h9vC6', 'M', NULL, '2020-05-12 07:27:45', '2020-05-12 07:27:45'),
(15, 'fdgdfgfdg', 'dfgdfgdfgdf@sd.fdgfdgf', NULL, '$2y$10$7N6zx3bo/G2UQjOUgElIGeWK5r9LgP1SIePRaEDuxDWfVNmFsAw.u', 'M', NULL, '2020-05-12 07:34:12', '2020-05-12 07:34:12'),
(16, 'fghfhgfh', 'gfhgfhgfhgf@sdsd.gfhfgfdg', NULL, '$2y$10$e2DFqFKoyb2ZD8OtCiyhwO1q/.sQBLX0p6tSZb166PZmol.57xW/S', 'M', NULL, '2020-05-12 07:34:53', '2020-05-12 07:34:53'),
(17, 'gfdfgfdgfd', 'gfdgdfgdfgdf@sdfdsf.dfgffsdgf', NULL, '$2y$10$Tm7MIClCcpF2iJezyHHv4OKheHrpt.Tl2./FZRCQGfa5kqvh78d4C', 'M', NULL, '2020-05-12 07:35:46', '2020-05-12 07:35:46'),
(18, 'sdfdsfds', 'sdfdsfdsf@defef.erew', NULL, '$2y$10$geuiG3tkeNCBD5VQB9dlFeJspj1ZPS3z.vAflsNSjXOcuwDMXYeGm', 'M', NULL, '2020-05-12 11:10:25', '2020-05-12 11:10:25'),
(19, 'dfgfdg', 'dfgdfgdfgdfgd@sdfedfdsf.dsfdsf', NULL, '$2y$10$RXh5yl/gNih7.5.Ef8H9HeV73Hzv0ipv3lTUX/.PnaCqBlQOietr6', 'M', NULL, '2020-05-12 11:17:18', '2020-05-12 11:17:18'),
(20, 'fdgfgfdgfdgfdgdfggf', 'dfgdfgdgdfg@sdfdsfds.sdffdfsd', NULL, '$2y$10$w2ZX2n3RXS/MEGWczRXBs.me/ySBKOJrpnT8nvYYGnPoXPxHf4ayi', 'M', NULL, '2020-05-12 11:21:03', '2020-05-12 11:21:03'),
(21, 'sddsfsdfsdf', 'fsdfsdfsfsd@asdasdad.com', NULL, '$2y$10$xTTfqv0QqaGUlBSPRb8QqO1bNh.BYEfHosnOPZpUzy.5RbsFORvGa', 'M', NULL, '2020-05-12 11:22:55', '2020-05-12 11:22:55'),
(22, 'gdfgdfgdfgdfgdf', 'dfgfdgdfgdf@sdtr.fghfh', NULL, '$2y$10$DIt06ejb0W3TUaEwnzs/Fu5z2xW0wnZSeJPrAymrz8YzXOwCUILfC', 'M', NULL, '2020-05-12 11:24:27', '2020-05-12 11:24:27'),
(23, 'dfgdfgdfgfdgdfg', 'dgggdg@sefsfsdfsdfdsf.sdfsdfsdfsd', NULL, '$2y$10$5.Y6nxZFnEwBZH6dwyepfO2KL7Ak/U78KJS0huKEs1rPMJYCHa.Aa', 'M', NULL, '2020-05-12 11:26:30', '2020-05-12 11:26:30'),
(24, 'dsfdsfds', 'tester@tv.com', NULL, '$2y$10$gfzKDH/AucIxPfxWnuPmEueArLbjVoCoC4Wxl4i9298WfVTiOReVO', 'M', NULL, '2020-05-17 08:03:56', '2020-05-17 08:03:56');

-- --------------------------------------------------------

--
-- Table structure for table `user_items`
--

DROP TABLE IF EXISTS `user_items`;
CREATE TABLE IF NOT EXISTS `user_items` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` double(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_item_ingredients`
--

DROP TABLE IF EXISTS `user_item_ingredients`;
CREATE TABLE IF NOT EXISTS `user_item_ingredients` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_item_id` int(11) NOT NULL,
  `ingredient_id` int(11) NOT NULL,
  `price` double(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
