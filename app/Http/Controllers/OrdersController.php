<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orders;
use App\User_items;
use App\Ingredients;
use App\User_item_ingredients;
use App\Pizzas;

class OrdersController extends Controller
{
    public $successStatus = 200;	

    public function create(Request $request){
        $input = $request->all(); 
        $orderArr = [];
        $orderArr['user_id'] = $input['user']['id'];
        $orderArr['order_no'] = $this->_genrateOrderNo();
        $orderArr['name'] = $input['user']['name'];
        $orderArr['email'] = $input['user']['email'];
        $orderArr['contact_number'] = $input['user']['number'];
        $orderArr['address'] = $input['user']['address'];   

        $orderPrice = 0;

		$ingredientsArr = Ingredients::all()->keyBy('id')->toArray();
        $itemsArr = [];
        $itemsIngrdArr = [];
		if(!empty($input['order'])){
			foreach ($input['order'] as $item){
				$user_items = [];
				$pizza = Pizzas::where('id',$item['id'])->orderBy('display_order', 'ASC')->first()->toArray();
				$user_items['user_id'] = $orderArr['user_id'];
				$user_items['item_id'] = $item['id'];
				$user_items['quantity'] = $item['quantity'];
				$user_items['price'] = $pizza['price'];
				$user_items['title'] = $pizza['title'];
				$itemsArr[] = $user_items;
				$orderPrice = $orderPrice + $pizza['price'] * $item['quantity'];
				if(!empty($item['ingredients'])){
					foreach ($item['ingredients'] as $ingId){
						$itemsIngrd = [];
						$itemsIngrd['ingredient_id'] = $ingId;
						$itemsIngrd['price'] = $ingredientsArr[$ingId]['price'];
						$itemsIngrdArr[$item['id']][] = $itemsIngrd;
						$orderPrice = $orderPrice + $ingredientsArr[$ingId]['price'] * $item['quantity'];
					}
				}
			}
		}
		$delivery_charge = config('app.DELIVERY_CHARGE');
		$orderArr['price'] = $orderPrice + $delivery_charge;
		$order_id = Orders::create($orderArr)->id;
		if(!empty($itemsArr)){
			foreach ($itemsArr as $item) {
				$item['order_id'] = $order_id;
				$user_item_id = User_items::create($item)->id;
				if(!empty($itemsIngrdArr[$item['item_id']])){
					foreach ($itemsIngrdArr[$item['item_id']] as $ingredient) {
						$ingredient['user_item_id'] = $user_item_id;
						User_item_ingredients::create($ingredient)->id;
					}
				}
			}
		}
        $success['status'] = 1;
        return response()->json(['success'=>$success], $this->successStatus); 
    }

    public function getOrders(Request $request){
        $user_id = $request->input('id');
    	$orders = Orders::where('user_id',$user_id)->get()->keyBy('id')->toArray();
    	$user_items = User_items::whereIn('order_id',array_keys($orders))->get()->toArray();
		foreach ($user_items as $user_item){
			$orders[$user_item['order_id']]['items'][] = $user_item;
		}
        return response()->json(['orders'=>$orders], $this->successStatus); 
    }

    public function _genrateOrderNo(){
    	$lastOrderNo = Orders::max('order_no');
    	if(empty($lastOrderNo)){ $lastOrderNo = 1000; }
    	return $lastOrderNo + 1;
    }

}
