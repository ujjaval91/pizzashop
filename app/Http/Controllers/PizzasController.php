<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Pizzas;
use App\Ingredients;
use Validator;

class PizzasController extends Controller
{
    public $successStatus = 200;	

    public function index(Request $request){
        $input = $request->all(); 
        $pizzasObj = new Pizzas();
    	// $pizzas = Pizzas::orderBy('display_order', 'ASC')->get();
    	if(!empty($input['id'])){
    		$pizzasObj = $pizzasObj->whereIn('id',$input['id']);
    	}
    	$pizzas = $pizzasObj->orderBy('display_order', 'ASC')->get();
    	$ingredients = Ingredients::orderBy('title', 'DESC')->get();
    	// $ingredients = Ingredients::all();
        $deliveryCharge = config('app.DELIVERY_CHARGE');
        return response()->json(['pizzas' => $pizzas,'ingredients' => $ingredients,'deliveryCharge' => $deliveryCharge], $this->successStatus); 
    }

    public function getpizza($id){
    	$pizza = Pizzas::find($id);
        return response()->json(['detail' => $pizza], $this->successStatus); 	
    }
    
}
