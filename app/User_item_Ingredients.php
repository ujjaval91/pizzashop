<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_item_ingredients extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_item_id', 'ingredient_id','price'
    ];
}
