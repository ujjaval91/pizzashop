<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Orders extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_no','name', 'email', 'user_id', 'contact_number','address','price'
    ];

    public function user_items()
    {
        return $this->hasMany(App\User_items::class);
    }
}
