<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_items extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'user_id','title', 'quantity','price'
    ];


    public function orders()
    {
        return $this->belongsTo(App\Orders::class);
    }
}
